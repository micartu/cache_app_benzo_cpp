package com.mlngroup.benzo_android;

import android.content.DialogInterface;
import android.icu.util.BuddhistCalendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.bearmonti.benzo_cache.Blcache;
import com.bearmonti.benzo_cache.User;
import com.bearmonti.benzo_cache.UserLoyality;

public class MainActivity extends AppCompatActivity {
    static {
        System.loadLibrary("benzo_cache_lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnCheckUser = (Button) findViewById(R.id.btnCheckUser);
        btnCheckUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUser();
            }
        });

        Button btnUpdateUser = (Button) findViewById(R.id.btnUpdateUser);
        btnUpdateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser();
            }
        });

        Button btnDeleteUser = (Button) findViewById(R.id.btnDeleteUser);
        btnDeleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteUser();
            }
        });

        String dbPath = this.getFilesDir().getAbsolutePath() + "/benzo.sqlite3";
        _cache =  Blcache.createWithPath(dbPath);
    }

    private void checkUser() {
        if (!_cache.existsUser(login)) {
            System.out.println("Have to create the user");
            UserLoyality l = new UserLoyality(login,
                    0,
                    login,
                    login,
                    login,
                    login,
                    login,
                    login);
            User u = new User(login,
                    login,
                    login,
                    login,
                    login,
                    login,
                    login,
                    login,
                    123,
                    "test@mail.ru",
                    login,
                    login,
                    0,
                    0,
                    l,
                    false,
                    "");
            _cache.createUser(u);
        } else {
            System.out.println("User was already created!");
            User u = _cache.getUser(login);
            System.out.println("Received user: " + u.toString());
        }
    }

    private void updateUser() {
        if (_cache.existsUser(login)) {
            String name = (oper == 0) ? "name" : "test";
            System.out.println("Have to update the user");
            UserLoyality l = new UserLoyality(login,
                    0,
                    login,
                    login,
                    login,
                    login,
                    login,
                    login);
            User u = new User(login,
                    name,
                    name,
                    login,
                    name,
                    login,
                    login,
                    name,
                    444,
                    "new_name@mail.ru",
                    login,
                    login,
                    0,
                    0,
                    l,
                    false,
                    "");
            _cache.updateUser(u);
            System.out.println("Updated the user...");
            oper++;
            if (oper > 1) {
                oper = 0;
            }
        }
    }

    private void deleteUser() {
        if (_cache.existsUser(login)) {
            _cache.deleteUser(login);
        }
    }

    private Integer oper = 0;
    private String login = "me_test";
    private Blcache _cache;
}
