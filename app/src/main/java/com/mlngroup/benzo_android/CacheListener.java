package com.mlngroup.benzo_android;

import com.bearmonti.benzo_cache.BcacheListener;
import com.bearmonti.benzo_cache.MapPatch;
import com.bearmonti.benzo_cache.MapPatchStations;
import com.bearmonti.benzo_cache.Transaction;
import com.bearmonti.benzo_cache.User;
import com.bearmonti.benzo_cache.WebPage;

public class CacheListener extends  BcacheListener {
    @Override
    public void userExistsCompleted(long id, boolean exists) {

    }

    @Override
    public void transactionExistsCompleted(long id, boolean exists) {

    }

    @Override
    public void webpageExistsCompleted(long id, boolean exists) {

    }

    @Override
    public void patchExistsCompleted(long id, boolean exists) {

    }

    @Override
    public void createUserCompleted(long id) {

    }

    @Override
    public void createTransactionCompleted(long id) {

    }

    @Override
    public void createWebpageCompleted(long id) {

    }

    @Override
    public void createPatchCompleted(long id) {

    }

    @Override
    public void createStationsCompleted(long id) {

    }

    @Override
    public void getUserCompleted(long id, User u) {

    }

    @Override
    public void getUserUnkCompleted(long id) {

    }

    @Override
    public void getTransactionCompleted(long id, Transaction t) {

    }

    @Override
    public void getTransactionUnkCompleted(long id) {

    }

    @Override
    public void getWebpageCompleted(long id, WebPage p) {

    }

    @Override
    public void getWebpageUnkCompleted(long id) {

    }

    @Override
    public void getPatchCompleted(long id, MapPatch p) {

    }

    @Override
    public void getPatchUnkCompleted(long id) {

    }

    @Override
    public void getPatchStationsCompleted(long id, MapPatchStations p) {

    }

    @Override
    public void updateUserCompleted(long id) {

    }

    @Override
    public void updateTransactionCompleted(long id) {

    }

    @Override
    public void updateWebpageCompleted(long id) {

    }

    @Override
    public void updatePatchCompleted(long id) {

    }

    @Override
    public void deleteUserCompleted(long id) {

    }

    @Override
    public void deleteTransactionCompleted(long id) {

    }

    @Override
    public void deleteWebpageCompleted(long id) {

    }

    @Override
    public void deletePatchCompleted(long id) {

    }
}
