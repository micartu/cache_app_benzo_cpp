//
//  benzo_cppTests.swift
//  benzo_cppTests
//
//  Created by Michael Artuerhof on 27.05.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import XCTest
import benzo_cache_sqlite_cpp_lib
@testable import benzo_cpp

let kTimeout: TimeInterval = 5

class benzoCacheTests: XCTestCase {
    override func setUp() {
        cache = Cache()
    }
    
    override func tearDown() {
    }
    
    func testUserCreation() {
        let exp = self.expectation(description: "User creation")
        let t = "test"
        let l = BMCUserLoyality.init(cpaCrdCode: t,
                                     cpaCrdRub: 0,
                                     cpaPrtCode: t,
                                     cpaPrtName: t,
                                     cpaPrtRef1: t,
                                     cpaPrtRef2: t,
                                     cpaPrtRef3: t,
                                     cpaPrtSubid: t)
        let u = BMCUser.init(login: t,
                             name: t,
                             phone: "01234",
                             surname: t,
                             midname: t,
                             avatar: "",
                             pin: t,
                             card: t,
                             birthday: 123,
                             email: t,
                             codeword: t,
                             favFuel: t,
                             balance: 100,
                             menuRefreshDate: 0,
                             loyality: l,
                             cardBlocked: false,
                             apn: t)
        self.cache.create(user: u) {
            self.cache.exists(login: t) { exists in
                XCTAssert(exists == true)
                self.cache.get(login: t) { u2 in
                    XCTAssert(u2?.login == u.login)
                    self.cache.delete(login: t) {
                        self.cache.exists(login: t) { exists in
                            XCTAssert(exists == false)
                            exp.fulfill()
                        }
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }
    
    var cache: Cache!
}
