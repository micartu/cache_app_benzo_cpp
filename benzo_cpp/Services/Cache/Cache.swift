//
//  Cache.swift
//  benzo_cache
//
//  Created by Michael Artuerhof on 24.05.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation
import benzo_cache_sqlite_cpp_lib

public class Cache: CacheService {
    public init() {
        listener = CacheListener()
        let p = Cache.pathForDB()
        let cacheUrl = p.appendingPathComponent("bcache.sqlite3").path
        cache = BMCBcache.create(withPath: cacheUrl, listener: listener)!
    }
    
    // MARK: - common methods
    public func exists(login: String, completion: @escaping ((Bool) -> Void)) {
        let id = cache.existsUser(login)
        listener.addOp(id, op: completion)
    }
    
    // MARK: - get methods
    public func get(login: String, completion: @escaping ((BMCUser?) -> Void)) {
        let id = cache.getUser(login)
        listener.addOp(id, op: completion)
    }
    
    // MARK: - create methods
    public func create(user: BMCUser, completion: @escaping (() -> Void)) {
        let id = cache.createUser(user)
        listener.addOp(id, op: completion)
    }
    
    // delete methods
    public func delete(login: String, completion: @escaping (() -> Void)) {
        let id = cache.deleteUser(login)
        listener.addOp(id, op: completion)
    }
    
    // MARK: - Private
    
    private static func pathForDB() -> URL {
        let fm = FileManager.default
        let docs = try! fm.url(for:.applicationSupportDirectory,
                               in: .userDomainMask,
                               appropriateFor: nil,
                               create: true)
        let u = docs.appendingPathComponent("db")
        if !fm.fileExists(atPath: u.path) {
            try! fm.createDirectory(at: u,
                                    withIntermediateDirectories: true,
                                    attributes: nil)
        }
        return u
    }

    private let cache: BMCBcache
    private let listener: CacheListener
}
