//
//  CacheUserService.swift
//  benzo_cpp
//
//  Created by Michael Artuerhof on 27.05.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation
import benzo_cache_sqlite_cpp_lib

public protocol CacheUserService {
    // common methods
    func exists(login: String, completion: @escaping ((Bool) -> Void))
    
    // create methods
    func create(user: BMCUser, completion: @escaping (() -> Void))
    
    // get methods
    func get(login: String, completion: @escaping ((BMCUser?) -> Void))
    
    // delete methods
    func delete(login: String, completion: @escaping (() -> Void))
}
