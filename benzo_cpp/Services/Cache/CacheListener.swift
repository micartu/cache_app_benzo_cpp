//
//  CacheListener.swift
//  benzo_cache
//
//  Created by Michael Artuerhof on 25.05.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation
import benzo_cache_sqlite_cpp_lib

class CacheListener {
    public func addOp(_ id: Int64, op: Any) {
        calls[id] = op
    }
    
    internal func put(_ op: @escaping (() -> Void)) {
        queue.addOperation { [weak self] in
            self?.lock.lock()
            self?.operations.append(op)
            self?.lock.unlock()
            self?.executeNext()
        }
    }
    
    // MARK: - Private
    
    private func executeNext() {
        queue.addOperation { [weak self] in
            let opp: (() -> Void)?
            self?.lock.lock()
            opp = self?.operations.popLast()
            self?.lock.unlock()
            if let op = opp {
                op()
            }
        }
    }
    
    private func pop(_ id: Int64) -> Any? {
        return calls.removeValue(forKey: id)
    }
    
    private func commonExistsCompleted(_ id: Int64,  exists: Bool) {
        if let closure = pop(id) as? ((Bool) -> Void) {
            put {
                closure(exists)
            }
        }
    }
    
    private func commonNoParamsCompleted(_ id: Int64) {
        if let closure = pop(id) as? (() -> Void) {
            put {
                closure()
            }
        }
    }

    private lazy var queue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Cache Listener Queue"
        queue.qualityOfService = .userInitiated
        return queue
    }()
    private lazy var operations = [() -> Void]()
    private var calls = [Int64: Any]()
    private let lock = NSLock()
}

extension CacheListener: BMCBcacheListener {
    public func userExistsCompleted(_ id: Int64, exists: Bool) {
        commonExistsCompleted(id, exists: exists)
    }
    
    public func transactionExistsCompleted(_ id: Int64, exists: Bool) {
    }
    
    public func webpageExistsCompleted(_ id: Int64, exists: Bool) {
    }
    
    public func patchExistsCompleted(_ id: Int64, exists: Bool) {
    }
    
    public func createUserCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func createTransactionCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func createWebpageCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func createPatchCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func createStationsCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func getUserCompleted(_ id: Int64, u: BMCUser) {
        if let closure = pop(id) as? ((BMCUser?) -> Void) {
            put {
                closure(u)
            }
        }
    }
    
    public func getUserUnkCompleted(_ id: Int64) {
        if let closure = pop(id) as? ((BMCUser?) -> Void) {
            put {
                closure(nil)
            }
        }
    }
    
    public func getTransactionCompleted(_ id: Int64, t: BMCTransaction) {
    }
    
    public func getTransactionUnkCompleted(_ id: Int64) {
    }
    
    public func getWebpageCompleted(_ id: Int64, p: BMCWebPage) {
    }
    
    public func getWebpageUnkCompleted(_ id: Int64) {
    }
    
    public func getPatchCompleted(_ id: Int64, p: BMCMapPatch) {
    }
    
    public func getPatchUnkCompleted(_ id: Int64) {
    }
    
    public func getPatchStationsCompleted(_ id: Int64, p: BMCMapPatchStations) {
    }
    
    public func updateUserCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func updateTransactionCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func updateWebpageCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func updatePatchCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func deleteUserCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func deleteTransactionCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func deleteWebpageCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
    
    public func deletePatchCompleted(_ id: Int64) {
        commonNoParamsCompleted(id)
    }
}
